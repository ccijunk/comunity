package com.chen.community.controller;


import com.chen.community.dto.PostsDTO;
import com.chen.community.mapper.UserMapper;
import com.chen.community.model.User;
import com.chen.community.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class IndexController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PostsService postsService;

    @GetMapping({"/", "/index"})
    public String index(HttpServletRequest resquest,
                        Model model

    ) {

        Cookie[] cookies =resquest.getCookies();
        if (cookies!=null && cookies.length!=0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token")) {
                    String token = cookie.getValue();
                    User user = userMapper.findByToken(token);
                    if (user != null) {
                        resquest.getSession().setAttribute("user", user);
                    }
                    break;
                }
            }
        }

        List<PostsDTO> postsdtolist = postsService.list();

        model.addAttribute("posts",postsdtolist);
        return "index";
    }

}