package com.chen.community.controller;

import com.chen.community.dto.AccessTokenDTO;
import com.chen.community.mapper.UserMapper;
import com.chen.community.model.User;
import com.chen.community.provider.GiteeProvider;
import com.chen.community.dto.GiteeUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;


@Controller
public class AuthorizeController {

    @Autowired
    private GiteeProvider giteeProvider;
    @Value("${gitee.client.id}")
    private String clientID;
    @Value("${gitee.client.secret}")
    private String clientSecret;
    @Value("${gitee.redirect.uri}")
    private String redirectUri;

    @Autowired
    private UserMapper userMapper;

    @GetMapping("/callback")
    String callback(@RequestParam(name = "code") String code
//                   , @RequestParam(name = "state") String state
                    , HttpServletRequest request
                    , HttpServletResponse response
                    ) {




        AccessTokenDTO accessTokenDTO = new AccessTokenDTO();
        accessTokenDTO.setCode(code);
        accessTokenDTO.setClient_id(clientID);
        accessTokenDTO.setRedirect_uri(redirectUri);
        accessTokenDTO.setClient_secret(clientSecret);
        String accessToken =giteeProvider.getAccessToken(accessTokenDTO);
        GiteeUser giteeUser =giteeProvider.getUser(accessToken);
        if (giteeUser!=null){
            //登录成功
            User user= new User();
            String token =UUID.randomUUID().toString();
            user.setAccountID(String.valueOf(giteeUser.getId()) );
            user.setName(giteeUser.getName());
            user.setToken(token);
            user.setGmtCreated(System.currentTimeMillis());
            user.setGmtModified(user.getGmtCreated());
            user.setAvatarUrl(giteeUser.getAvatarUrl());
            userMapper.insert(user);
            response.addCookie(new Cookie("token",token));
//            request.getSession().setAttribute("user", giteeUser);
            return "redirect:index";
        }
        else{
            //登录失败
            return "redirect:index";
        }


    }
}
