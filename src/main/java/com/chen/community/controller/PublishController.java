package com.chen.community.controller;

import com.chen.community.mapper.PostMapper;
import com.chen.community.mapper.UserMapper;
import com.chen.community.model.Posts;
import com.chen.community.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Controller
public class PublishController {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PostMapper postMapper;
    @GetMapping("/publish")
    public String pubilish(HttpServletRequest resquest){

        Cookie[] cookies =resquest.getCookies();
        if(cookies!=null && cookies.length!=0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token")) {
                    String token = cookie.getValue();
                    User user = userMapper.findByToken(token);
                    if (user != null) {
                        resquest.getSession().setAttribute("user", user);
                    }
                    break;
                }
            }
        }
        return "publish";
    }




    @PostMapping("/publish")
    public String doPublish(@RequestParam(value = "title",required = false) String title, //这里看看前端传来的参数是什么 ,这里required看看是什么意思
                            @RequestParam(value = "description",required = false) String description,
                            @RequestParam(value = "tag",required = false) String tag,
                            Model model,
                                    HttpServletRequest resquest

                            ){


        model.addAttribute("title", title);
        model.addAttribute("description",description);
        model.addAttribute("tag",tag);

        if(title == null || title ==""){
            model.addAttribute("error","标题不能为空" );
            return "publish";
        }
        if(description == null || description ==""){
            model.addAttribute("error","内容不能为空" );
            return "publish";
        }
        if(tag == null || tag ==""){
            model.addAttribute("error","标签不能为空" );
            return "publish";
        }


        User user =null;

        Cookie[] cookies =resquest.getCookies();
        if(cookies!=null && cookies.length!=0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token")) {
                    String token = cookie.getValue();
                    user = userMapper.findByToken(token);
                    if (user != null) {
                        resquest.getSession().setAttribute("user", user);
                    }
                    break;
                }
            }
        }

        if (user==null){
            model.addAttribute("error","用户还没登陆" );
            return "publish";
        }

        Posts posts = new Posts();
        posts.setTitle(title);
        posts.setCreator(user.getId());
        posts.setDescription(description);
        posts.setTag(tag);
        posts.setGmtCreated(System.currentTimeMillis());
        posts.setGmtModified(posts.getGmtCreated());
        postMapper.create(posts);


        return "publish";





    }

}
