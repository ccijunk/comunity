package com.chen.community.dto;

import com.chen.community.model.User;
import lombok.Data;

@Data
public class PostsDTO {
        private Integer id;
        private String title;
        private String description;
        private String tag;
        private Integer creator;
        private long gmtCreated;
        private long gmtModified;
        private Integer viewCount;
        private Integer commentCount;
        private Integer likeCount;
        private User user;
}
