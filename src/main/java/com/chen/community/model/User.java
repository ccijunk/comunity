package com.chen.community.model;

import lombok.Data;

@Data
public class User {
    private int id;
    private String accountID;
    private String name;
    private String token;
    private long gmtCreated;
    private long gmtModified;
    private String avatarUrl;

}
