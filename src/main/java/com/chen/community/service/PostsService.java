package com.chen.community.service;

import com.chen.community.dto.PostsDTO;
import com.chen.community.mapper.PostMapper;
import com.chen.community.mapper.UserMapper;
import com.chen.community.model.Posts;
import com.chen.community.model.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostsService {


    @Autowired
    PostMapper postMapper;
    @Autowired
    UserMapper userMapper;


    public List<PostsDTO> list() {

        List<Posts> postsList =  postMapper.list();
        List<PostsDTO> postsDTOList = new ArrayList<>();
        for(Posts post:postsList){
            User user = userMapper.findByID(post.getCreator());
            PostsDTO postsDTO = new PostsDTO();
            BeanUtils.copyProperties(post,postsDTO);
            postsDTO.setUser(user);
            postsDTOList.add(postsDTO);
        }

        return postsDTOList;
    }
}
