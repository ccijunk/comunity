package com.chen.community.provider;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chen.community.dto.AccessTokenDTO;
import com.chen.community.dto.GiteeUser;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class GiteeProvider {
    public String getAccessToken(AccessTokenDTO accessTokenDTO){
        MediaType JSONa = MediaType.get("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient();
//?grant_type=authorization_code&code={code}&client_id={client_id}&redirect_uri={redirect_uri}&client_secret={client_secret}

        RequestBody body = RequestBody.create(JSONa, JSON.toJSONString(accessTokenDTO));

        String url = String.format("https://gitee.com/oauth/token?grant_type=authorization_code&code=%s&client_id=%s&redirect_uri=%s&client_secret=%s",
                accessTokenDTO.getCode(),
                accessTokenDTO.getClient_id(),
                accessTokenDTO.getRedirect_uri(),
                accessTokenDTO.getClient_secret());
//        System.out.println(url);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            String ressult = response.body().string();
//            System.out.println(ressult);
//           result的格式 {"access_token":"1fe99f012f0557d88b7ec2b156e2efa0","token_type":"bearer","expires_in":86400,"refresh_token":"6542f71f5b083995c5b4ed26bdae2aeacc7aef11d7e19c5535a946dda83ea21a","scope":"user_info","created_at":1638695188}
            JSONObject jsonobject = JSONObject.parseObject(ressult);
            return jsonobject.getString("access_token");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public GiteeUser getUser(String accesssToken){
        OkHttpClient client = new OkHttpClient();


            Request request = new Request.Builder()
                    .url("https://gitee.com/api/v5/user?access_token="+accesssToken)
                    .build();

            try (Response response = client.newCall(request).execute()) {

                String result = response.body().string();
//                System.out.println(result);
                GiteeUser giteeUser = JSON.parseObject(result,GiteeUser.class);
//                System.out.println(giteeUser);
                return giteeUser;

            } catch (IOException e) {
                e.printStackTrace();
            }
        return null;

    }

}
