package com.chen.community.mapper;


import com.chen.community.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


@Mapper
public interface UserMapper {


    //    默认去get方法去取变量
    @Insert("INSERT into USER (account_id,name,token,gmt_created,gmt_modifid,avatar_url) values (#{accountID},#{name},#{token},#{gmtCreated},#{gmtModified},#{avatarUrl})")
    void insert(User user);

//    如果不是一个类那么就要加上参数
    @Select("select * from USER where token = #{token}")
    User findByToken(@Param("token") String token);  //这里查询到的表格会自动填充成一个类，跟fastjson一样？   属性要驼峰转换

    @Select("select * from USER where id = #{id}")
    User findByID(@Param("id") Integer id);  //这里查询到的表格会自动填充成一个类，跟fastjson一样？   属性要驼峰转换

}