package com.chen.community.mapper;

import com.chen.community.model.Posts;
import com.chen.community.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PostMapper {


    @Insert("INSERT into POST (id,title,description,creator,gmt_created,gmt_modified,tag) values (#{id},#{title},#{description},#{creator},#{gmtCreated},#{gmtModified},#{tag})")
    void create(Posts posts);

    @Select("select * from POST")
    List<Posts> list();  //这里查询到的表格会自动变成一个列表吗
}
