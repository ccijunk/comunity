create table POST
(
    id int auto_increment,
    title VARCHAR(50),
    description text,
    gmt_created bigint,
    gmt_modified bigint,
    creator int,
    comment_count int default 0,
    view_count int default 0,
    like_count int default 0,
    tag varchar(256)
);
