create table USER
(
    ID INT auto_increment,
    ACCOUNT_ID VARCHAR(100),
    NAME VARCHAR(50),
    TOKEN CHAR(36),
    GMT_CREATED BIGINT,
    GMT_MODIFID BIGINT
);

